<?php

function opr2_coeliakia_group($name, $bundle) {
  switch ($name) {
  case 'group_inline':
    return _opr2grp('inline');

  case 'group_dgmode':
    return _opr2grp('category', 'Diagnózis módja');
  case 'group_dgsym':
    return _opr2grp('category', 'Tünetek a diagnóziskor, társult betegségek');
  case 'group_dgsym_malabs':
  case 'group_dgsym_diarr':
  case 'group_dgsym_lactit':
  case 'group_dgsym_abdpain':
  case 'group_dgsym_const':
  case 'group_dgsym_othergi':
  case 'group_dgsym_weight':
  case 'group_dgsym_growth':
  case 'group_dgsym_puberty':
  case 'group_dgsym_amenorr':
  case 'group_dgsym_derma':
  case 'group_dgsym_othsym':
  case 'group_dgsym_anemia':
  case 'group_dgsym_hypopralb':
  case 'group_dgsym_coagapath':
  case 'group_dgsym_abnormliv':
  case 'group_dgsym_othlab':
  case 'group_dgsym_comorb':
  case 'group_dgsym_diab':
  case 'group_dgsym_thyr':
  case 'group_dgsym_aimm':
  case 'group_dgsym_other':
    return _opr2grp('conditional', '');

  case 'group_abod':
    return _opr2grp('category', 'Antitest eredmények');
  case 'group_abod_igatga':
  case 'group_abod_igaema':
  case 'group_abod_iggtga':
  case 'group_abod_igaema':
  case 'group_abod_igadgp':
  case 'group_abod_iggdgp':
  case 'group_abod_igasum':
    return _opr2grp('conditional', '');

  case 'group_hist':
    return _opr2grp('category', 'Szövettani eredmények');
  case 'group_hist_cond':
    return _opr2grp('conditional', '');
  case 'group_hist_res':
    return _opr2grp('div', 'Szövettani vélemény');
  case 'group_hist_totvatrop':
  case 'group_hist_p3vatrop':
  case 'group_hist_p2vatrop':
  case 'group_hist_p1vatrop':
  case 'group_hist_normvil':
    return _opr2grp('conditional', '');

  case 'group_hladq':
    return _opr2grp('category', 'HLA-DQ tipizálási eredmény');
  case 'group_hladq_cond':
    return _opr2grp('conditional', '');

  case 'group_sage':
    return _opr2grp('category', 'SAGE score');

  case 'group_comp':
    return _opr2grp('category', 'Szövődmények');
  case 'group_comp_diab':
  case 'group_comp_thyr':
  case 'group_comp_osteo':
  case 'group_comp_tumor':
  case 'group_comp_other':
  case 'group_comp_death':
    return _opr2grp('conditional', '');
  }
}
