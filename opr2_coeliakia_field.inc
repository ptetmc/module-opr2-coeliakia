<?php

function opr2_coeliakia_field($name) {
  switch ($name) {
  case 'field_dgmode_vatrop':
    $field = _opr2fld('boolean_radio', 'Coeliakiára jellemző boholyatrophia fennállása szövettanilag dokumentált', array('required' => TRUE));
    $field['instance']['description'] = 'legalább partialis II illetve subtotalis vagy totalis boholyatrophia, ahol a boholy/crypta arány 1 vagy kisebb, más nomenklatura szerint Marsh III';
    return $field;
  case 'field_dgmode_endabod':
    return _opr2fld('boolean_radio', 'Pozitív endomysium antitest eredmény dokumentált', array('required' => TRUE));
  case 'field_dgmode_tglabod':
    return _opr2fld('boolean_radio', 'Pozitív transzglutamináz antitest eredmény dokumentált', array('required' => TRUE));
  case 'field_dgmode_tglabodh':
    return _opr2fld('boolean_radio', 'Magas pozitív (>10xULN) szérum transzglutamináz antitest eredmény dokumentált', array('required' => TRUE));
  case 'field_dgmode_fam':
    return _opr2fld('boolean_radio', 'Családi előfordulás', array('required' => TRUE));
  case 'field_dgmode_suminst':
    return _opr2fld('number_decimal', 'Diagnózis összegzés a klinikai centrum szerint', array('required' => TRUE));
  case 'field_dg_inst':
    return _opr2fld('select_or_other', 'A coeliakia diagnózist felállító intézmény');
  case 'field_dg_date':
    return _opr2fld('date_partial_day', 'A diagnózis kimondásának ideje', array('required' => TRUE));
  case 'field_dgsym_clin':
    return _opr2fld('boolean_radio', 'Van-e coeliakiának tulajdonítható klinikai tünet ?', array('required' => TRUE));
  case 'field_dgsym_lab':
    return _opr2fld('boolean_radio', 'Van-e coeliakiának tulajdonítható laboratóriumi eltérés ?', array('required' => TRUE));
  case 'field_dgsym_malabs':
    return _opr2fld('boolean_radio', 'Generalizált felszívódási zavar (malabszorpciós szindróma)');
  case 'field_dgsym_malabs_since':
  case 'field_dgsym_diarr_since':
  case 'field_dgsym_lactit_since':
  case 'field_dgsym_abdpain_since':
  case 'field_dgsym_const_since':
  case 'field_dgsym_othergi_since':
  case 'field_dgsym_weight_since':
  case 'field_dgsym_growth_since':
  case 'field_dgsym_puberty_since':
  case 'field_dgsym_amenorr_since':
  case 'field_dgsym_derma_since':
  case 'field_dgsym_othsym_since':
  case 'field_dgsym_anemia_since':
  case 'field_dgsym_hypopralb_since':
  case 'field_dgsym_coagapath_since':
  case 'field_dgsym_abnormliv_since':
    return _opr2fld('number_list', 'fennállás ideje', array(
      'options' => array(
        1 => '6 hónapnál rövidebb ideje',
        2 => '6 hó - 2 éve',
        3 => '2 évnél régebben',
        4 => 'nem ismert',
      ),
    ));
  case 'field_dgsym_diarr':
    return _opr2fld('boolean_radio', 'Visszatérő hasmenés');
  case 'field_dgsym_lactit':
    return _opr2fld('boolean_radio', 'Laktóz intolerancia');
  case 'field_dgsym_abdpain':
    return _opr2fld('boolean_radio', 'Visszatérő hasi fájdalom');
  case 'field_dgsym_const':
    return _opr2fld('boolean_radio', 'Székrekedés');
  case 'field_dgsym_othergi':
    return _opr2fld('boolean_radio', 'Egyéb emésztőszervi tünet (haspuffadás, emésztési zavar, hasi diszkomfort)');
  case 'field_dgsym_weight':
    return _opr2fld('boolean_radio', 'Nem megfelelő súlyfejlődés, súlyvesztés');
  case 'field_dgsym_growth':
    return _opr2fld('boolean_radio', 'Növekedési zavar, alacsony termet');
  case 'field_dgsym_puberty':
    return _opr2fld('boolean_radio', 'Késő serdülés');
  case 'field_dgsym_amenorr':
    return _opr2fld('boolean_radio', 'Amenorrhea');
  case 'field_dgsym_derma':
    return _opr2fld('boolean_radio', 'Bőrtünet, ami dermatitis herpetiformisnak megfelelhet');
  case 'field_dgsym_derma_immfl':
    return _opr2fld('boolean_radio', 'igazolták-e bőr immunfluoreszcens vizsgálattal a dermatitis herpetifromis fennállását');
  case 'field_dgsym_derma_date':
    return _opr2fld('date_partial_day', 'bőrbiopsia ideje');
  case 'field_dgsym_derma_inst':
    return _opr2fld('institute', 'bőrbiopsia helye');
  case 'field_dgsym_othsym':
    return _opr2fld('boolean_radio', 'Egyéb tünet');
  case 'field_dgsym_othsym_detail':
    return _opr2fld('text', 'mi');
  case 'field_dgsym_anemia':
    return _opr2fld('boolean_radio', 'Vashiány, vérszegénység');
  case 'field_dgsym_hypopralb':
    return _opr2fld('boolean_radio', 'Hypoproteinaemia, hypalbuminaemia');
  case 'field_dgsym_coagapath':
    return _opr2fld('boolean_radio', 'Alacsony prothrombin, véralvadási zavar');
  case 'field_dgsym_abnormliv':
    return _opr2fld('boolean_radio', 'Kóros májfunkciós értékek');
  case 'field_dgsym_othlab':
    return _opr2fld('boolean_radio', 'Egyéb coeliakiára/felszívódási zavarra utaló laboratóriumi eltérés');
  case 'field_dgsym_othlab_detail':
  case 'field_dgsym_aimm_type':
  case 'field_dgsym_other_type':
    return _opr2fld('text', 'mi');
  case 'field_dgsym_comorb':
    return _opr2fld('boolean_radio', 'Társult betegségek');
  case 'field_dgsym_diab':
    return _opr2fld('boolean_radio', '1-es típusú diabetes mellitus');
  case 'field_dgsym_diab_start':
  case 'field_dgsym_thyr_start':
  case 'field_dgsym_aimm_start':
  case 'field_dgsym_other_start':
    return _opr2fld('date_partial_day', 'kezdete');
  case 'field_dgsym_thyr':
    return _opr2fld('boolean_radio', 'Pajzsmirigybetegség');
  case 'field_dgsym_thyr_type':
    return _opr2fld('select_or_other', 'típusa', array(
      'options' => array(
        1 => 'hypothyreosis',
        2 => 'hyperthyresosis',
        3 => 'antitest pozitivitás',
      ),
    ));
  case 'field_dgsym_aimm':
    return _opr2fld('boolean_radio', 'Egyéb autoimmun betegség');
  case 'field_dgsym_seliga':
    return _opr2fld('boolean_radio', 'Szelektív IgA hiány');
  case 'field_dgsym_down':
    return _opr2fld('boolean_radio', 'Down szindróma');
  case 'field_dgsym_turner':
    return _opr2fld('boolean_radio', 'Turner szindróma');
  case 'field_dgsym_other':
    return _opr2fld('boolean_radio', 'Egyéb');

  case 'field_abod_igatga':
    return _opr2fld('boolean_radio', 'Szérum IgA Transglutaminase antitest vizsgálat a kezelés előtt');
  case 'field_abod_igaema':
    return _opr2fld('boolean_radio', 'Szérum IgA EMA vizsgálat a kezelés előtt');
  case 'field_abod_iggtga':
    return _opr2fld('boolean_radio', 'Szérum IgG Transglutaminase antitest vizsgálat vizsgálat a kezelés előtt');
  case 'field_abod_igaema':
    return _opr2fld('boolean_radio', 'Szérum IgG EMA vizsgálat a kezelés előtt');
  case 'field_abod_igadgp':
    return _opr2fld('boolean_radio', 'Szérum IgA deamidált gliadin peptid elleni antitest (DGP) vizsgálat a kezelés előtt');
  case 'field_abod_iggdgp':
    return _opr2fld('boolean_radio', 'Szérum IgG deamidált gliadin peptid elleni antitest (DGP) vizsgálat a kezelés előtt');
  case 'field_abod_igatga_date':
  case 'field_abod_igaema_date':
  case 'field_abod_iggtga_date':
  case 'field_abod_igaema_date':
  case 'field_abod_igadgp_date':
  case 'field_abod_iggdgp_date':
    return _opr2fld('date_partial_day', 'vizsgálat ideje');
  case 'field_abod_igatga_inst':
  case 'field_abod_igaema_inst':
  case 'field_abod_iggtga_inst':
  case 'field_abod_igaema_inst':
  case 'field_abod_igadgp_inst':
  case 'field_abod_iggdgp_inst':
    return _opr2fld('institute', 'vizsgálatot végző intézmény/labor');
  case 'field_abod_igatga_kit':
  case 'field_abod_igaema_kit':
  case 'field_abod_iggtga_kit':
  case 'field_abod_igaema_kit':
  case 'field_abod_igadgp_kit':
  case 'field_abod_iggdgp_kit':
    return _opr2fld('select_or_other', 'kit');
  case 'field_abod_igatga_res':
  case 'field_abod_igaema_res':
  case 'field_abod_iggtga_res':
  case 'field_abod_igaema_res':
  case 'field_abod_igadgp_res':
  case 'field_abod_iggdgp_res':
    return _opr2fld('number_decimal', 'vizsgálati eredmény', array(
      'suffix' => 'egység',
    ));
  case 'field_abod_igatga_eval':
  case 'field_abod_igaema_eval':
  case 'field_abod_iggtga_eval':
  case 'field_abod_igaema_eval':
  case 'field_abod_igadgp_eval':
  case 'field_abod_iggdgp_eval':
    return _opr2fld('number_list', 'vizsgálati eredmény értékelése', array(
      'options' => array(
        1 => 'negatív',
        2 => 'pozitív',
        3 => 'magas pozitív (10x normálérték felett)',
      ),
    ));

  case 'field_abod_latertype':
    return _opr2fld('number_list', 'Szérum antitest vizsgálat típusa a későbbiekben', array(
      'options' => array(
        1 => 'kezelés előtti',
        2 => 'glutenmentes diéta utáni',
        3 => 'glutenterhelés utáni',
      ),
    ));
  case 'field_abod_igasum':
    return _opr2fld('number_list', 'Szérum össz IgA szint', array(
      'options' => array(
        1 => 'normális',
        2 => 'alacsony (0.2g/l alatt)',
        3 => 'nem vizsgálták',
      ),
    ));
  case 'field_abod_igasum_unit':
    return _opr2fld('number_list', 'Szérum össz IgA szint mértékegysége', array(
      'options' => array(
        1 => 'g/l',
        2 => 'mg/dl',
      ),
    ));
  case 'field_hist':
    return _opr2fld('boolean_radio', 'Volt-e szövettani vizsgálat a vékonybélből vagy a duodenumből?');
  case 'field_hist_inst':
    return _opr2fld('institute', 'Biopsiát végző intézmény');
  case 'field_hist_date':
    return _opr2fld('date_partial_day', 'Biopsia ideje');
  case 'field_hist_type':
    return _opr2fld('number_list', 'Biopsia típusa', array(
      'options' => array(
        1 => 'kezelés előtti (kezdeti)',
        2 => 'glutenmentes diéta után',
        3 => 'glutenterhelés után',
      ),
    ));
  case 'field_hist_id':
    return _opr2fld('text', 'Lelet száma');
  case 'field_hist_num':
    return _opr2fld('number_list', 'Minták száma', array(
      'options' => array(
        1 => '1 db',
        2 => 'több minta',
      ),
    ));

  case 'field_hist_totvatrop':
    return _opr2fld('boolean_radio', 'Subtotalis vagy totalis boholyatrophia');
  case 'field_hist_p3vatrop':
    return _opr2fld('boolean_radio', 'Partialis III. boholyatrophia');
  case 'field_hist_p2vatrop':
    return _opr2fld('boolean_radio', 'Partialis II. boholyatrophia');
  case 'field_hist_p1vatrop':
    return _opr2fld('boolean_radio', 'Partialis I. boholyatrophia');
  case 'field_hist_normvil':
    return _opr2fld('boolean_radio', 'Megtartott boholyszerkezet');
  case 'field_hist_totvatrop_ori':
  case 'field_hist_p3vatrop_ori':
  case 'field_hist_p2vatrop_ori':
  case 'field_hist_p1vatrop_ori':
  case 'field_hist_normvil_ori':
    return _opr2fld('number_list', 'Milyen a minta orientáiója?', array(
      'options' => array(
        1 => 'megfelelő',
        2 => 'nem megfelelő',
        3 => 'nem szerepel a leleten',
      ),
    ));
  case 'field_hist_totvatrop_ielym':
  case 'field_hist_p3vatrop_ielym':
  case 'field_hist_p2vatrop_ielym':
  case 'field_hist_p1vatrop_ielym':
  case 'field_hist_normvil_ielym':
    return _opr2fld('number_list', 'Milyen az intraepithelialis lymphocyta szám?', array(
      'options' => array(
        1 => 'emelkedett (25/100 hámsejt vagy magasabb)',
        2 => 'normális (25/100 hámsejt alatt)',
        3 => 'nem szerepel a leleten',
      ),
    ));
  case 'field_hist_totvatrop_bcr':
  case 'field_hist_p3vatrop_bcr':
  case 'field_hist_p2vatrop_bcr':
  case 'field_hist_p1vatrop_bcr':
  case 'field_hist_normvil_bcr':
    return _opr2fld('number_list', 'Mennyi a boholy-crypta arány?', array(
      'options' => array(
        1 => '1 vagy kisebb',
        2 => '1-2 között',
        3 => '2-3 között',
        4 => '3 vagy magasabb',
      ),
    ));
  case 'field_hist_totvatrop_tg2':
  case 'field_hist_p3vatrop_tg2':
  case 'field_hist_p2vatrop_tg2':
  case 'field_hist_p1vatrop_tg2':
  case 'field_hist_normvil_tg2':
    return _opr2fld('number_list', 'TG2-specifikus IgA lerakódás a mintában', array(
      'options' => array(
        1 => 'van',
        2 => 'nincs',
        3 => 'nem történt ilyen vizsgálat',
      ),
    ));
  case 'field_hist_res_file':
    return _opr2fld('file', 'Feltöltés: fel kívánja.-e tölteni file formájában a leletet vagy az ezt tartalmazó zárójelentést?');

  case 'field_hladq':
    return _opr2fld('boolean_radio', 'HLA-DQ tipizálás történt');
  case 'field_hladq_inst':
    return _opr2fld('institute', 'HLA-DQ tipizálást végző intézmény');
  case 'field_hladq_res':
    return _opr2fld('select_or_other', 'HLA-DQ tipizálási eredmény', array(
      'options' => array(
        1 => 'DQ2 heterodimer van',
        2 => 'DQ8 heterodimer van',
        3 => 'DQ2 és DQ8 heterodimer van',
        4 => 'incomplet DQ2',
        5 => 'nincs sen DQ2 sem DQ8',
      ),
    ));
  case 'field_hladq_all':
    return _opr2fld('select_or_other', 'HLA-DQ allélek bevitele');

  case 'field_sage_ss':
    return _opr2fld('number_integer', 'Symptom (SS)');
  case 'field_sage_as':
    return _opr2fld('number_integer', 'Antibody (AS)');
  case 'field_sage_gs':
    return _opr2fld('number_integer', 'Genetic markers, HLA (GS)');
  case 'field_sage_es':
    return _opr2fld('number_integer', 'Endoscopy/Histology (ES)');
  case 'field_sage_res':
    return _opr2fld('number_integer', 'SAGE score');

  case 'field_comp_diab':
    return _opr2fld('boolean_radio', '1-es típusú diabetes mellitus');
  case 'field_comp_thyr':
    return _opr2fld('boolean_radio', 'Pajzsmirigybetegség');
  case 'field_comp_osteo':
    return _opr2fld('boolean_radio', 'Csonttörés, osteoporosis');
  case 'field_comp_tumor':
    return _opr2fld('boolean_radio', 'Rosszindulatú daganat');
  case 'field_comp_other':
    return _opr2fld('boolean_radio', 'Egyéb szövődmény');
  case 'field_comp_death':
    return _opr2fld('boolean_radio', 'Halál');
  case 'field_comp_diab_date':
  case 'field_comp_thyr_date':
  case 'field_comp_osteo_date':
  case 'field_comp_tumor_date':
  case 'field_comp_other_date':
  case 'field_comp_death_date':
    return _opr2fld('date_partial_day', 'ideje');
  case 'field_comp_death_reason':
    return _opr2fld('select_or_other', 'oka', array(
      'options' => array(
        1 => 'baleset',
        2 => 'rosszindulatú daganat',
      ),
    ));
  }
}

// replace: select options: '<,'>s/\(\d\+\)=\([^,]*\),\?/\1 => '\2',^M/g
// replace: number_list: '<,'>s/_opr2fld('boolean_radio', \('.*'\))/_opr2fld('number_list', \1, array(
//  'options' => array(
//  ),))/
