<?php

function opr2_coeliakia_bundle_register_coeliakia_core() {
  $bundle = array(
    'machine_name' => 'register_coeliakia_core',
    'instances' => array(
      'field_form_version' => array(
        'default_value' => array(array('value'=>'v1')),
      ),
    ), // end instances

    'groups' => array(
    ), // end groups

    'tree' => array(
      'field_form_version',

      'field_inpatient',

      'group_inline' => array(
        'group_dgmode' => array(
          'field_dgmode_vatrop',
          'field_dgmode_endabod',
          'field_dgmode_tglabod',
          'field_dgmode_tglabodh',
          'field_dgmode_fam',
          'field_dgmode_suminst',
        ),

        'field_dg_inst',
        'field_dg_date',

        'group_dgsym' => array(
          'field_dgsym_clin',
          'field_dgsym_lab',

          'group_dgsym_malabs' => array(
            'field_dgsym_malabs',
            'field_dgsym_malabs_since',
          ),
          'group_dgsym_diarr' => array(
            'field_dgsym_diarr',
            'field_dgsym_diarr_since',
          ),
          'group_dgsym_lactit' => array(
            'field_dgsym_lactit',
            'field_dgsym_lactit_since',
          ),
          'group_dgsym_abdpain' => array(
            'field_dgsym_abdpain',
            'field_dgsym_abdpain_since',
          ),
          'group_dgsym_const' => array(
            'field_dgsym_const',
            'field_dgsym_const_since',
          ),
          'group_dgsym_othergi' => array(
            'field_dgsym_othergi',
            'field_dgsym_othergi_since',
          ),

          'group_dgsym_weight' => array(
            'field_dgsym_weight',
            'field_dgsym_weight_since',
          ),
          'group_dgsym_growth' => array(
            'field_dgsym_growth',
            'field_dgsym_growth_since',
          ),
          'group_dgsym_puberty' => array(
            'field_dgsym_puberty',
            'field_dgsym_puberty_since',
          ),
          'group_dgsym_amenorr' => array(
            'field_dgsym_amenorr',
            'field_dgsym_amenorr_since',
          ),
          'group_dgsym_derma' => array(
            'field_dgsym_derma',
            'field_dgsym_derma_since',
            'field_dgsym_derma_immfl',
            'field_dgsym_derma_date',
            'field_dgsym_derma_inst',
          ),
          'group_dgsym_othsym' => array(
            'field_dgsym_othsym',
            'field_dgsym_othsym_since',
            'field_dgsym_othsym_detail',
          ),

          'group_dgsym_anemia' => array(
            'field_dgsym_anemia',
            'field_dgsym_anemia_since',
          ),
          'group_dgsym_hypopralb' => array(
            'field_dgsym_hypopralb',
            'field_dgsym_hypopralb_since',
          ),
          'group_dgsym_coagapath' => array(
            'field_dgsym_coagapath',
            'field_dgsym_coagapath_since',
          ),
          'group_dgsym_abnormliv' => array(
            'field_dgsym_abnormliv',
            'field_dgsym_abnormliv_since',
          ),
          'group_dgsym_othlab' => array(
            'field_dgsym_othlab',
            'field_dgsym_othlab_detail',
          ),

          'group_dgsym_comorb' => array(
            'field_dgsym_comorb',
            'group_dgsym_diab' => array(
              'field_dgsym_diab',
              'field_dgsym_diab_start',
            ),
            'group_dgsym_thyr' => array(
              'field_dgsym_thyr',
              'field_dgsym_thyr_type',
              'field_dgsym_thyr_start',
            ),
            'group_dgsym_aimm' => array(
              'field_dgsym_aimm',
              'field_dgsym_aimm_type',
              'field_dgsym_aimm_start',
            ),

            'field_dgsym_seliga',
            'field_dgsym_down',
            'field_dgsym_turner',
            'group_dgsym_other' => array(
              'field_dgsym_other',
              'field_dgsym_other_type',
              'field_dgsym_other_start',
            ),
          ),
        ),

        'group_abod' => array(
          'group_abod_igatga' => array(
            'field_abod_igatga',
            'field_abod_igatga_date',
            'field_abod_igatga_inst',
            'field_abod_igatga_kit',
            'field_abod_igatga_res',
            'field_abod_igatga_eval',
          ),

          'group_abod_igaema' => array(
            'field_abod_igaema',
            'field_abod_igaema_date',
            'field_abod_igaema_inst',
            'field_abod_igaema_kit',
            'field_abod_igaema_res',
            'field_abod_igaema_eval',
          ),

          'group_abod_iggtga' => array(
            'field_abod_iggtga',
            'field_abod_iggtga_date',
            'field_abod_iggtga_inst',
            'field_abod_iggtga_kit',
            'field_abod_iggtga_res',
            'field_abod_iggtga_eval',
          ),

          'group_abod_igaema' => array(
            'field_abod_igaema',
            'field_abod_igaema_date',
            'field_abod_igaema_inst',
            'field_abod_igaema_kit',
            'field_abod_igaema_res',
            'field_abod_igaema_eval',
          ),

          'group_abod_igadgp' => array(
            'field_abod_igadgp',
            'field_abod_igadgp_date',
            'field_abod_igadgp_inst',
            'field_abod_igadgp_kit',
            'field_abod_igadgp_res',
            'field_abod_igadgp_eval',
          ),

          'group_abod_iggdgp' => array(
            'field_abod_iggdgp',
            'field_abod_iggdgp_date',
            'field_abod_iggdgp_inst',
            'field_abod_iggdgp_kit',
            'field_abod_iggdgp_res',
            'field_abod_iggdgp_eval',
          ),

          'field_abod_latertype',

          'group_abod_igasum' => array(
            'field_abod_igasum',
            'field_abod_igasum_unit',
          ),
        ),

        'group_hist' => array(
          'group_hist_cond' => array(
            'field_hist',
            'field_hist_inst',
            'field_hist_date',
            'field_hist_type',
            'field_hist_id',
            'field_hist_num',

            'group_hist_res' => array(
              'group_hist_totvatrop' => array(
                'field_hist_totvatrop',
                'field_hist_totvatrop_ori',
                'field_hist_totvatrop_ielym',
                'field_hist_totvatrop_bcr',
                'field_hist_totvatrop_tg2',
              ),

              'group_hist_p3vatrop' => array(
                'field_hist_p3vatrop',
                'field_hist_p3vatrop_ori',
                'field_hist_p3vatrop_ielym',
                'field_hist_p3vatrop_bcr',
                'field_hist_p3vatrop_tg2',
              ),

              'group_hist_p2vatrop' => array(
                'field_hist_p2vatrop',
                'field_hist_p2vatrop_ori',
                'field_hist_p2vatrop_ielym',
                'field_hist_p2vatrop_bcr',
                'field_hist_p2vatrop_tg2',
              ),

              'group_hist_p1vatrop' => array(
                'field_hist_p1vatrop',
                'field_hist_p1vatrop_ori',
                'field_hist_p1vatrop_ielym',
                'field_hist_p1vatrop_bcr',
                'field_hist_p1vatrop_tg2',
              ),

              'group_hist_normvil' => array(
                'field_hist_normvil',
                'field_hist_normvil_ori',
                'field_hist_normvil_ielym',
                'field_hist_normvil_bcr',
                'field_hist_normvil_tg2',
              ),

              'field_hist_res_file',
            ),
          ),
        ), // end group_hist

        'group_hladq' => array(
          'group_hladq_cond' => array(
            'field_hladq',
            'field_hladq_inst',
            'field_hladq_res',
            'field_hladq_all',
          ),
        ),

        'group_sage' => array(
          'field_sage_ss',
          'field_sage_as',
          'field_sage_gs',
          'field_sage_es',

          'field_sage_res',
        ),

        'group_comp' => array(
          'group_comp_diab' => array(
            'field_comp_diab',
            'field_comp_diab_date',
          ),
          'group_comp_thyr' => array(
            'field_comp_thyr',
            'field_comp_thyr_date',
          ),
          'group_comp_osteo' => array(
            'field_comp_osteo',
            'field_comp_osteo_date',
          ),
          'group_comp_tumor' => array(
            'field_comp_tumor',
            'field_comp_tumor_date',
          ),
          'group_comp_other' => array(
            'field_comp_other',
            'field_comp_other_date',
          ),

          'group_comp_death' => array(
            'field_comp_death',
            'field_comp_death_date',
            'field_comp_death_reason',
          ),
        ), // end group_comp
      ), // end group_inline
    ), // end tree
  );

  return $bundle;
}

// replace: add group for all fields
// :'<,'>s/'field_\(.*\)',/'group_\1' => array(^M\0^M'field_\1_since',^M),/g
